/*
** ETNA PROJECT, 17/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/



int my_strcmp(const char *s1, const char *s2)
{
    int i = 0;
    int res = 0;
    while (s1[i] != '\0' && s2[i] != '\0' ){
        if (s1[i] == s2[i]){
            i++;
            continue;
        } else  break;
    }
    res = s1[i] - s2[i];
    return res;
}
