/*
** ETNA PROJECT, 18/03/2020 by bellou_a
** [...]
** File description:
**      [...]
*/

int my_strlen(const char *str)
{
    int len = 0;
    while (str[len] != '\0')
        len++;
    return len;
}

char *my_strcat(char *dest, const char *src)
{
    int j = 0;
    int lendest=my_strlen(dest);
    int lensrc = my_strlen(src);

    for(j=0; j<=lensrc; j++)
        dest[lendest+j]=src[j];
    return dest;
}
