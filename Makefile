##
## ETNA PROJECT, 21/03/2020 by bellou_a
## [...]
## File description:
##      [...]
##
CC = gcc
NAME = libmy.a
SRC =   lib/my/my_putchar.c \
        lib/my/my_putstr.c  \
        lib/my/my_getnbr.c  \
        lib/my/my_putnbr.c  \
        lib/my/my_isneg.c   \
        lib/my/my_swap.c    \
        lib/my/my_strcpy.c  \
        lib/my/my_strncpy.c \
        lib/my/my_strcmp.c  \
        lib/my/my_strncmp.c \
        lib/my/my_strcat.c  \
        lib/my/my_strncat.c \
        lib/my/my_strstr.c  \
        lib/my/my_strdup.c
OBJ = $(SRC:%.c=%.o)
LIB = ar r $(NAME) $(OBJ)
RAN = ranlib $(NAME)
RM = rm -f
CP = cp $(NAME) ./lib/
$(NAME):	$(OBJ)
			$(LIB)
			$(RAN)
			$(CP)
			$(RM) $(NAME)
all:		$(NAME)

delete:
			$(RM) ./lib/$(NAME)
clean:		delete
			$(RM) $(OBJ)
fclean:		clean
			$(RM) $(NAME)
re: fclean all
